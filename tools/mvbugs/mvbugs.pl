#!/usr/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# ***** BEGIN LICENSE BLOCK *****
# Version: MPL 1.1
#
# The contents of this file are subject to the Mozilla Public License Version
# 1.1 (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
# http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS IS" basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
# for the specific language governing rights and limitations under the
# License.
#
# The Original Code is Bugzilla Bug Mover.
#
# The Initial Developer of the Original Code is Olav Vitters.
# Portions created by Olav Vitters are Copyright (C) 2005 Olav Vitters. All
# Rights Reserved.
#
# Contributor(s): Holger Schurig <holgerschurig@nikocity.de>
#                 Terry Weissman <terry@mozilla.org>
#                 Dan Mosedale <dmose@mozilla.org>
#                 Dave Miller <justdave@syndicomm.com>
#                 Zach Lipton  <zach@zachlipton.com>
#                 Jake <jake@acutex.net>
#                 Bradley Baetz <bbaetz@cs.mcgill.ca>
#                 Christopher Aillon <christopher@aillon.com>
#
# ***** END LICENSE BLOCK *****
#
# What it does:
# Moves bugs on SQL level. Updates bugnrs, even in the descriptions. Used to
# move the Evolution bugs from http://bugzilla.ximian.com/ to
# http://bugzilla.gnome.org/
#
# Ximian was using Bugzilla 2.10.0, Gnome was at 2.16.0. This script only moves
# the data required to move the Ximian data. More tables likely need moving for
# newer Bugzilla versions.
#
# Notice:
#  - The group moving was never finished
#  - Creates users without *any* permissions
#  - Run it first on a test installation

$| = 1; # unbuffered STDOUT for nice progress info

###########################################################################
# Variables that can be changed
###########################################################################

use diagnostics;
use strict;
use DBI qw(neat_list);

# Source db 
my $my_db_name1 = "bugsximian";
my $my_db_host1 = "localhost";
my $my_db_port1 = 3306;
my $my_db_user1 = "bugsximian";
my $my_db_pass1 = '';
my $my_db_url1  = 'http://bugzilla.ximian.com/';
my $my_db_url1_re  = 'http://bug(?:s|zilla)\.ximian\.com/'; # regexp.. because ximian had 2 urls

# Dest db
my $my_db_name2 = "bugsnew";
my $my_db_host2 = "localhost";
my $my_db_port2 = 3306;
my $my_db_user2 = "bugs";
my $my_db_pass2 = '';
my $my_db_url2  = 'http://bugzilla.gnome.org/';

###########################################################################
# Connect to the databases
###########################################################################

my $db_base = 'mysql';

# get a handle to the low-level DBD driver
my $drh = DBI->install_driver($db_base)
    or die "Can't connect to the $db_base. Is the database installed and up and running?\n";

# Connect source database
my $connectstring1 = "dbi:$db_base:$my_db_name1:host=$my_db_host1:port=$my_db_port1";
my $dbh1 = DBI->connect($connectstring1, $my_db_user1, $my_db_pass1, { RaiseError => 1 } )
    or die "Can't connect to the table '$connectstring1'.\n",
           "Have you read the Bugzilla Guide in the doc directory?  Have you read the doc of '$db_base'?\n";


# Connect destination database
my $connectstring2 = "dbi:$db_base:$my_db_name2:host=$my_db_host2:port=$my_db_port2";
my $dbh2 = DBI->connect($connectstring2, $my_db_user2, $my_db_pass2, { RaiseError => 1 } )
    or die "Can't connect to the table '$connectstring2'.\n",
           "Have you read the Bugzilla Guide in the doc directory?  Have you read the doc of '$db_base'?\n";

END {
    $dbh1->disconnect if $dbh1;
    $dbh2->disconnect if $dbh2;
}



# WHERE string on bugs table
#
# Selects the bugs that have to be moved.
#
# This where string is used to retrieve the bugids. These bugids are used
# thoughout the program. The where string is also changed into
# $where_components for the components table. 
#
# Note: where string should only contain products (some other code depends on
# this)
my $where; # XXX -- change this
$where = "(product = 'evolution' OR product = 'GtkHtml' OR product = 'GAL' OR product = 'Evolution-Data-Server' OR product = 'evolution-sharp' OR product = 'Soup' OR product = 'Connector')";
my $where_components = $where;
$where_components =~ s/\bproduct\b/program/g; # ugly :)

# Need the bugnrs for the other queries
# (MySQL is slow when using join all the time)
print "Fetching bug_ids: ";
my @bugids = @{$dbh1->selectcol_arrayref("SELECT bug_id FROM bugs WHERE $where", { Columns => [1] })};
my $where_bugids = sprintf(" IN (%s)", join(",", @bugids));

print scalar(@bugids) . " done\n";

# Where to find userids that match the bugs that need to be moved
# Note:
#   The actual update of these fields is done elsewhere. When adding new fields,
#   make sure these will actually be updated (rename_ variables below)
my @user_sql = (
    "SELECT DISTINCT assigned_to FROM bugs WHERE $where",
    "SELECT DISTINCT reporter FROM bugs WHERE $where",
    "SELECT DISTINCT qa_contact FROM bugs WHERE $where",
    "SELECT DISTINCT who FROM cc WHERE bug_id $where_bugids",
    "SELECT DISTINCT who FROM bugs_activity WHERE bug_id $where_bugids",
    "SELECT DISTINCT who FROM longdescs WHERE bug_id $where_bugids",
    "SELECT DISTINCT submitter_id FROM attachments WHERE bug_id $where_bugids",
    "SELECT DISTINCT initialowner FROM components WHERE $where_components",
    "SELECT DISTINCT initialqacontact FROM components WHERE $where_components"
);

# Use for example: mysql -H -u user databasename < auditsql1.sql
# the -H produces nice html lists
# XXX -- I want the ablility to ignore these queries (and the bugs listed by them)
my %auditsql; # check if bugs can be moved.. query should only return results on audit failure
$auditsql{"Bugs marked duplicate of another bug that will not be moved"} = <<EOF;
SELECT   dupe, dupe_of, b.product AS "dupe product", a.product AS "dupe_of product", a.component
FROM     duplicates
  LEFT JOIN bugs a ON duplicates.dupe_of = a.bug_id
  LEFT JOIN bugs b ON duplicates.dupe = b.bug_id
WHERE    dupe $where_bugids
  AND    dupe_of NOT $where_bugids
EOF

$auditsql{"Bug that won't be moved marked duplicate of bug that will be moved"} = <<EOF;
SELECT   dupe_of, dupe, b.product AS "dupe_of product", a.product AS "dupe product", a.component
FROM     duplicates
  LEFT JOIN bugs a ON duplicates.dupe = a.bug_id
  LEFT JOIN bugs b ON duplicates.dupe_of = b.bug_id
WHERE    dupe NOT $where_bugids
  AND    dupe_of $where_bugids
EOF

$auditsql{"Bug blocks non-moving bug"} = <<EOF;
SELECT       dependson, blocked, b.product AS "dependson product", a.product AS "blocked product", a.component
FROM         dependencies
  LEFT JOIN  bugs a ON dependencies.blocked = a.bug_id
  LEFT JOIN  bugs b ON dependencies.dependson = b.bug_id
WHERE        dependson $where_bugids
  AND        (blocked NOT $where_bugids)
ORDER BY     dependson
EOF

$auditsql{"Bug depends on non-moving bug"} = <<EOF;
SELECT       blocked, dependson, b.product AS "blocked product", a.product AS "dependson product", a.component
FROM         dependencies
  LEFT JOIN  bugs a ON dependencies.dependson = a.bug_id
  LEFT JOIN  bugs b ON dependencies.blocked = b.bug_id
WHERE        blocked $where_bugids
  AND        (dependson NOT $where_bugids)
ORDER BY     blocked
EOF


# CompareTables will verify these tables to ensure all fields exist in the
# destination database
# 
# This has to be done because of the way move_records() works. It moves all
# fields. Therefore all fields of the source database must exist in the
# destination database.
my @tables_to_compare = qw{
    attachments bugs bugs_activity cc components dependencies
    duplicates fielddefs groups keyworddefs keywords longdescs
    milestones products profiles versions
};

# Ximian specific... converts severity into the Gnome priority XXX -- or was it the other way around?
my %convertToSeverity = (
    Critical=>'critical',
    Blocker=>'blocker',
    Major=>'major',
    Normal=>'normal',
    Minor=>'minor',
    Cosmetic=>'trivial',
    Wishlist=>'enhancement'
);

# Number to add to original bugid
# set to undef to use next available number
my $bugid_increment = 200000;


# profiles
my %sUseridToName; # source db: userid -> login_name
my %dUsernameToId; # dest   db: login_name -> userid
# fielddefs
my %sFieldidToName; # source db: fieldid -> fieldname
my %dFieldnameToId; # dest   db: fieldname -> fieldid

my %dGroupnameToId; # dest   db: groupname -> groupid

my %profilesaccess; # hash on login_name, returns list/array of groups user has access to
my @old_userids; # List of userids in source database
# keyworddefs
my %sKeywordidToName; # source db: id -> keyword
my %dKeywordnameToId; # dest   db: keyword -> id

# WARNING: following variable will contain dummy values, don't rely on it!
my %dKeywordidToName; # dest   db: id -> keyword (determines what keywordids exist)

# otn means Old database To New database
my $otn_bugs; # source bugid -> dest bugid
my $otn_attachments; # source attachmentid -> dest attachmentid

#######################
# rename variables
#
# These define per table how a field needs to be updated for the destination
# database. key: field name, value = function that returns the new value
my %rename_bugs = (
    assigned_to=>\&fixUserid,
    reporter=>\&fixUserid,
    qa_contact=>\&fixUserid,
    rep_platform=>\&retAll,
    priority=>\&fixPriorityAndSeverity,
    bug_id=>\&addNumberToBugid
);
my %rename_attachments = (
    bug_id=>\&fixBugid,
    submitter_id=>\&fixUserid
);
my %rename_cc = (
    bug_id=>\&fixBugid,
    who=>\&fixUserid
);
my %rename_profiles = (
# XXX - no access by default
#    groupset=>\&fixGroupset,
#    blessgroupset=>\&fixGroupset
    groupset=>\&retZero,
    blessgroupset=>\&retZero
);
my %rename_bugs_activity = (
    fieldid=>\&fixFieldid, # XXX -- maybe fix removed/added here too
    bug_id=>\&fixBugid,
    who=>\&fixUserid,
    attach_id=>\&fixAttachid
);
my %rename_dependencies = (
    blocked=>\&fixBugid,
    dependson=>\&fixBugid
);
my %rename_duplicates = (
    dupe_of=>\&fixBugid,
    dupe=>\&fixBugid
);
my %rename_keywords = (
    bug_id=>\&fixBugid,
    keywordid=>\&fixKeywordid
);
my %rename_components = (
    initialowner=>\&fixUserid,
    initialqacontact=>\&fixUserid
);
my %rename_longdescs = (
    bug_id=>\&fixBugid,
    thetext=>\&fixDescriptions,
    who=>\&fixUserid
);
###########################################################################
# Functions taken from other Bugzilla files
###########################################################################
#
# These have been copied from other Bugzilla files because
# 1. The functions have been slightly modified
# 2. Allows the script to work without relying on other files

# Notice: Taken from globals.pl
sub lsearch {
    my ($list,$item) = (@_);
    my $count = 0;
    foreach my $i (@$list) {
        if ($i eq $item) {
            return $count;
        }
        $count++;
    }
    return -1;
}

# Notice: Taken from checksetup.pl
# After copying made the following change: s/dbh/dbh2/
sub GroupDoesExist ($)
{
    my ($name) = @_;
    my $sth = $dbh2->prepare("SELECT name FROM groups WHERE name='$name'");
    $sth->execute;
    if ($sth->rows) {
        return 1;
    }
    return 0;
}

# Notice: Taken from checksetup.pl
#
# This subroutine checks if a group exist. If not, it will be automatically
# created with the next available bit set
# Made the following changes from the original:
#  * Add groups with isbuggroup set to 1
#  * s/dbh/dbh2/
sub AddBugGroup {
    my ($name, $desc, $userregexp) = @_;
    $userregexp ||= "";

    return if GroupDoesExist($name);

    # get highest bit number
    my $sth = $dbh2->prepare("SELECT bit FROM groups ORDER BY bit DESC");
    $sth->execute;
    my @row = $sth->fetchrow_array;

    # normalize bits
    my $bit;
    if (defined $row[0]) {
        $bit = $row[0] << 1;
    } else {
        $bit = 1;
    }

    print "  Adding group $name ...\n";
    $sth = $dbh2->prepare('INSERT INTO groups
                          (bit, name, description, userregexp, isbuggroup)
                          VALUES (?, ?, ?, ?, ?)');
    $sth->execute($bit, $name, $desc, $userregexp, 1);
    return $bit;
}


###########################################################################
# SQL functions
###########################################################################

# Selects a field from multiple sql statements
#
# This is like the UNION statement
sub SelectUnion {
    my ($fetchRef, $where, $print) = @_;

    $print ||= "";
    
    my @fetch = @$fetchRef;
    my %result;
    my ($column222, $table);
    
    my $sql;
    # get highest bit number
    foreach my $sql (@fetch) {
        print $print;

        my $sth = $dbh1->prepare($sql);
        $sth->execute;
        
        my @row;
        while ( @row = $sth->fetchrow_array ) {
            $result{$row[0]} = undef;
        }
    }
    # return results
    return keys %result;
}

# Run SQL queries to check the integrity of the database
sub AuditSQL {
    
    my $success = 1;
    my $count = 0;
    while ((my $errormsg, my $sql) = each %auditsql) {
        $count += 1;
        my $sth = $dbh1->prepare($sql);
        if ($sth->execute) {
            print "WARNING: $errormsg. Run mysql < auditsql$count.sql for a nice list\n";
            open FILE, ">auditsql$count.sql" or die "Couldn't open auditsql file";
            print FILE "$sql";
            close FILE;
            $success = 0;
        }
        $sth->finish(); # finish is required if not all results are fetched
    }
    return $success;
}

# Determine what groups have been used 
#
#
sub DetermineActiveGroups {
    my ($where) = @_;

    my @row;
    
    # XXX -- also check groupset in bug_activity for possibly usage of different groups!!
    
    # Put all groups in a hash
    my %groups = @{$dbh1->selectcol_arrayref("SELECT bit, name FROM groups WHERE isbuggroup = 1",
                                           { Columns=>[1,2] })};

    my $sql;
    
    # Determine nr of bugs per group
    print "Fetching group usage:\n";
    while ((my $key, my $val) = each %groups) {
        $sql = "SELECT COUNT(*) FROM bugs WHERE groupset & $key != 0 AND $where";
        my $sth = $dbh1->prepare($sql);
        $sth->execute;
        my ($count) = $sth->fetchrow_array;
        print "  $val\t$count\n";
        
        # Remove (ignore) all groups with 0 bugs
        # these won't be created in the destination db
        if ($count == "0") {
            delete $groups{$key}
        }
    }
    print "Groups: " . join(", ", values(%groups)) . "\n";

    return \%groups;
}

# Determine what groups a user has access to
#
# returns a hash of login_name -> list/array of group names
sub DetermineProfileGroup {
    my ($groups2, $userids, $field) = @_;

    my $sql;
    my %access;
    
    print "Fetching $field access: ";
    $sql = sprintf("SELECT login_name FROM profiles WHERE $field & ? != 0 AND userid IN (%s)",
                      join(",", @$userids)
    );
    my $sth = $dbh1->prepare($sql);

    while ((my $key, my $val) = each %$groups2) {
        $sth->execute($key);
        
        my @row;
        while ( @row = $sth->fetchrow_array ) {
            $access{$row[0]} = [] if not exists $access{$row[0]};
            
            push (@{$access{$row[0]}}, $val);
        }
        print ".";
    }
    print "\n";
    return \%access;
}

# Copies groups from source database to destination
#
sub MoveGroups {
    my (@groupnames) = @_;

    if (scalar(@groupnames) == 0) {
        return;
    }

    my $sql = sprintf("SELECT name, description, userregexp FROM groups WHERE name IN (%s) ORDER BY bit",
                      join(",", map($dbh2->quote($_), @groupnames))
    );
    my $sth = $dbh1->prepare($sql);
    $sth->execute;
    while (my $data = $sth->fetchrow_hashref) {
        AddBugGroup($data->{name}, $data->{description}, $data->{userregexp});
    }
}

# move_records moves records from one database to another
# args are:
#   dbh1          source database handler
#   dbh2          destination database handler
#   table         name of the table to move
#   keyname       uniq field name.. will maybe be removed
#   rename_funcs  hash of field name->rename function
#   where         where string to use in the sql
# 
# Note:
# 1. Where string might not be enough due to fields needed in other tables.
#    Fix that yourself
# 2. Read the code to understand how rename_funcs work
# 3. Code will try to move *all* fields over. It does NOT check if all fields
#    exist in the destination table. This is compared elsewhere.
# 4. Use MakeWhere if not all fields should be moved over (existing products)
sub move_records {
    my ($dbh1, $dbh2, $table, $keyname, $rename_funcs, $where, $ignore) = @_;
    
    # Create SELECT statement for old database
    my $sql1;
    if ($where) {
        $sql1 = sprintf "SELECT * FROM %s WHERE %s", $table, $where;
    } else {
        $sql1 = sprintf "SELECT * FROM %s", $table;
    };
    my $sth1 = $dbh1->prepare($sql1);
    $sth1->execute();
    
    # Analyse fields based upon the old database
    my (@fields);
    
    @fields = @{$sth1->{NAME}};
    my %fieldidx = %{$sth1->{NAME_hash}};
    print "Moving table $table [ Nr of fields: " . scalar @fields;
    
    # Determine the auto_increment column
    my $auto_increment_col = lsearch($sth1->{'mysql_is_auto_increment'}, 1);
    if ($auto_increment_col != -1) {
        print ", increment column: " . $fields[$auto_increment_col];
    }

    print " ]\n";
    
    # Create INSERT statement for new database
    my $sql2 = sprintf("INSERT INTO %s (%s) VALUES (%s)",
                       $table,
                       join(",", @fields),
                       join(",", ("?")x@fields)
    );
    my $sth2 = $dbh2->prepare($sql2);
    # Convert $keyname into a row index
    if ($auto_increment_col == -1) {
        if ($keyname && exists $fieldidx{$keyname} ) {
            $keyname = $fieldidx{$keyname};
        } else {
            $keyname = undef;
        }
    }
    
    
    # Now move the bugs over
    my %newkeys;
    my $i = 0;
    my $forced_autoincr;
    if ($auto_increment_col == -1) {
        $forced_autoincr = undef;
    }
    while ( my @row = $sth1->fetchrow_array ) {
        my $idx;
        my $uniqkey = undef;

        # Show progress every 500 records (taken from checksetup.pl)
        print "$i..." if !($i++ % 500);
        
        # Remember the auto_increment value and set it to undef so that
        # the destination database gives it a new number
        if ($auto_increment_col != -1) {
            $uniqkey = $row[$auto_increment_col];
            $row[$auto_increment_col] = undef;
        } elsif ( $keyname ) {
            $uniqkey = $row[$keyname];
            $uniqkey = undef;
        }
        
        # Call field update functions
        # 
        # WARNING:
        # Although a function is given a reference to the record, a function
        # should only access columns that are NOT updated by any other
        # function.
        # Not following above advise will give unpredictable results (the value
        # might already be updated by the other function).
        while ((my $field, my $renfunc) = each %$rename_funcs) {
            $idx = $fieldidx{$field};
            
            $row[$idx] = &$renfunc($row[$idx], $field, \@row, \%fieldidx, $uniqkey);
        }
        if ($auto_increment_col != -1) {
            $forced_autoincr = $row[$auto_increment_col];
        }
        $sth2->execute(@row);
        # Fetch last_insert_id and store it in a hash
        if ($forced_autoincr) {
            $newkeys{$uniqkey} = $forced_autoincr;
        } elsif ($auto_increment_col != -1) {
              $newkeys{$uniqkey} = $sth2->{'mysql_insertid'}
        }
    }
    print "\n" if $i != 0;
    return \%newkeys;
}

# XXX -- compare tables:
# 1. Compare if all fields from source database exist in the destination
# 2. Compare the Enum columns in the bug table
#    XXX -- maybe integrate that with #1
#        -- or not ... some Enums might be updated differently
#        I'll maybe hack that in it
sub CompareTables {
    my (%src, %dst);
    
    my $success = 1;
    
    print "Tables to compare: " . join(", ", @tables_to_compare) . "\n";
    foreach my $table (@tables_to_compare) {
        print "  Checking: $table";
        %src = @{$dbh1->selectcol_arrayref("DESCRIBE $table",
                                           { Columns=>[1,2] })};
        %dst = @{$dbh2->selectcol_arrayref("DESCRIBE $table",
                                           { Columns=>[1,2] })};
        
        while ((my $key, my $val) = each %src) {
            if (!exists $dst{$key}) {
                print "\nWARNING: Source column $key of table $table does NOT exists in destination table!";
                $success = 0;
            } else {
                print "\n    Field $key\n        OLD=$val\n        NEW=$dst{$key}" if $val ne $dst{$key};
            }
        }
        print "\n";
    }
    return $success;
}

# Creates a where string usable for move_records
# WARNING: returns undef if no records have to be moved!!
sub MakeWhere {
    my ($pri_field_dst_def, $table_def, $uniq_field_src_def, $table_compare, $field_src, $where) = @_;

    # Now compare fielddefs
    # 1. Get the field names in destination database
    my @dst_values = @{$dbh2->selectcol_arrayref("SELECT $pri_field_dst_def FROM $table_def",
                           { Columns=>[1] })};
    # 2. Get the fieldids that do not exist in destination database
    my @src_values = @{$dbh1->selectcol_arrayref(
                sprintf("SELECT $uniq_field_src_def FROM $table_def WHERE $pri_field_dst_def NOT IN (%s)",
                        join(",", map($dbh1->quote($_), @dst_values))),
                { Columns=>[1] }
    )};
    if (scalar @src_values == 0) {
        # All fielddefs of src database exist in destination database
        return undef;
    }
    # 3. Determine which of those values have actually been used
    my $sql = <<EOF;
SELECT DISTINCT $field_src
FROM   $table_compare
WHERE  $where
  AND  $field_src IN (%s)
EOF
#    print "SQL : $sql\n";
#    print "src_values: " . join(",", map($dbh1->quote($_), @src_values)) . "\n";
    my @new_values = @{$dbh1->selectcol_arrayref(
            sprintf($sql, join(",", map($dbh1->quote($_), @src_values))), 
            { Columns=>[1] }
    )};
    if (scalar @new_values == 0) {
        # None of the new values have actually been used, ignore them
        return undef;
    }
    return $uniq_field_src_def . " IN (" . join(",", map($dbh1->quote($_), @new_values)) . ")";
}


sub UpdateProfiles {
    my (@userids) = @_;
    
    my $sql = sprintf("SELECT login_name FROM profiles WHERE userid IN (%s)",
                      join(",", @userids)
    );
    my @login_names = @{$dbh1->selectcol_arrayref($sql, { Columns=>[1] })};
    my $where2 = sprintf("login_name IN (%s)",
                        join(",", map($dbh1->quote($_), @login_names))
    );
    # inefficient, but saves some code
    my $profiles_where = MakeWhere("login_name", "profiles", "login_name", "profiles", "login_name", $where2);

    print "www\n";
    return if !$profiles_where;
    print "yes: $profiles_where\n";

    move_records ($dbh1, $dbh2, "profiles", "userid", \%rename_profiles, $profiles_where);
}

# Set user hashes
sub SetUserHashes {
    my (@userids) = @_;

    # XXX -- login_name where can be used from UpdateProfiles
    my $sql = sprintf("SELECT userid, login_name FROM profiles WHERE userid IN (%s)",
                      join(",", @userids)
    );
    %sUseridToName = @{$dbh1->selectcol_arrayref($sql, { Columns=>[1,2] })};
    $sql = sprintf("SELECT LCASE(login_name), userid FROM profiles WHERE login_name IN (%s)",
                   join(",", map($dbh2->quote($_), values %sUseridToName))
    );
    %dUsernameToId = @{$dbh2->selectcol_arrayref($sql, { Columns=>[1,2] })};
}

# Compare the fielddefs tables between the source and destination databases
sub UpdateFielddefs {
    my $fielddefs_where = MakeWhere("name", "fielddefs", "fieldid", "bugs_activity", "fieldid", "bug_id " . $where_bugids);

    return if !$fielddefs_where;

    move_records($dbh1, $dbh2, "fielddefs", undef, undef, $fielddefs_where);
}

sub setFielddefHashes {
    %sFieldidToName = @{$dbh1->selectcol_arrayref("SELECT fieldid, name FROM fielddefs",
                                                  { Columns=>[1,2] })};
    %dFieldnameToId = @{$dbh2->selectcol_arrayref("SELECT name, fieldid FROM fielddefs",
                                                  { Columns=>[1,2] })};
}

# Copy keyworddefs that are used in keywords table
sub UpdateKeyworddefs {
    
    %dKeywordidToName = @{$dbh2->selectcol_arrayref("SELECT id, name FROM keyworddefs",
                                                    { Columns=>[1,2] })};
    
    my $keyworddefs_where = MakeWhere("name", "keyworddefs", "id", "keywords", "keywordid", "bug_id " . $where_bugids);
    
    return if !$keyworddefs_where;
   
    # Field 'id' doesn't have auto_increment, fixKeyworddefid hacks around it (see checksetup.pl)
    move_records($dbh1, $dbh2, "keyworddefs", undef, { id=>\&fixKeyworddefid }, $keyworddefs_where);
}

sub setKeyworddefHashes {
    %sKeywordidToName = @{$dbh1->selectcol_arrayref("SELECT id, name FROM keyworddefs",
                                                    { Columns=>[1,2] })};
    %dKeywordnameToId = @{$dbh2->selectcol_arrayref("SELECT LCASE(name), id FROM keyworddefs",
                                                    { Columns=>[1,2] })};
}

###########################################################################
# Functions that change one or move fields (used indirectly by move_records)
###########################################################################

# Return destination bugid for given source bugid
#
# for usage by move_records
sub fixBugid {
    my ($val, $field, $rowRef, $fieldidxRef, $uniq) = @_;
    
    my $n = $otn_bugs->{$val};

    if (!$n) {
        print "WARNING: bugid $val unknown?!? Bug $uniq\n";
    }
    
    return $n;
}

# Return destination userid for given source userid
#
# for usage by move_records
sub fixUserid {
    my ($val, $field, $rowRef, $fieldidxRef, $uniq) = @_;
    
    # Can be 0 for empty qa_contact
    return 0 if (!$val || $val eq "0");

    my $n = $dUsernameToId{lc($sUseridToName{$val})};

    if (!$n) {
        print "WARNING: userid $val unknown?!? Bug $uniq\n";
    }
    
    return $n;
}

# Add specific number to uniq value
sub addNumberToBugid {
    my ($val, $field, $rowRef, $fieldidxRef, $uniq) = @_;

    if ($bugid_increment) {
        return $uniq + $bugid_increment;
        
    }

    return $val;
}

# Return destination db attachmentid for given source db attachmentid
sub fixAttachid {
    my ($val, $field, $rowRef, $fieldidxRef, $uniq) = @_;
    
    if (!$val) {return $val} # could be NULL in bugs_activity
    
    my $n = $otn_attachments->{$val};

    if (!$n) {
        print "WARNING: attachid $val unknown?!?\n";
    }
    
    return $n;
}

# Change the groupset field based upon global %profilesaccess
#
# %profilesaccess determines what groupnames a login_name has access to
# function determines the groupset bits to use in the destination
# database and sets groupset field accordingly
sub fixGroupset {
    my ($val, $field, $rowRef, $fieldidxRef) = @_;
    # field value, field name, row, fieldidx

    # Determine login_name (email address)
    my $login_name = $rowRef->[$fieldidxRef->{login_name}];

    my $newxs = 0; # Default xs = 0
    foreach my $xs (map($dGroupnameToId{$_}, @{$profilesaccess{$field}->{$login_name}})) {
        $newxs = 0+$newxs | 0+$xs;
    }
    
    return $newxs;
}

# Update fieldid (defined in fielddefs)
sub fixFieldid {
    my ($val, $field, $rowRef, $fieldidxRef) = @_;

    return $dFieldnameToId{$sFieldidToName{$val}};
}

# Generate unique keyworddefid
#
# The keywordid in the keyworddefs table lacks auto_increment. Instead the code
# must find the lowest unused number. The code below simulates that.
sub fixKeyworddefid {
    my ($val, $field, $rowRef, $fieldidxRef) = @_;
    
    # Find unused number
    my $i = 1;
    while (exists $dKeywordidToName{$i}) {$i++}

    # Pretend that the insert is going to succeed
    $dKeywordidToName{$i} = $i; # dummy
    
    return $i;
}

# Update keywordid (defined in keyworddefs)
sub fixKeywordid {
    my ($val, $field, $rowRef, $fieldidxRef) = @_;

    return $dKeywordnameToId{lc($sKeywordidToName{$val})};
}

# Originally a copy of quoteUrls, modified to suit mvbugs.pl
sub fixDescriptions ($$$$$) {
    my ($text, $field, $rowRef, $fieldidxRef, $uniq) = @_;

    return $text unless $text;
    
    # $uniq refers to bug_id
    # $otn_bugs:        source bugid        -> dest bugid
    # $otn_attachments: source attachmentid -> dest attachmentid

    my $count = 0;

    # Now, quote any "#" characters so they won't confuse stuff later
    $text =~ s/#/%#/g;

    # Next, find anything that looks like a URL or an email address and
    # pull them out the the text, replacing them with a "##<digits>##
    # marker, and writing them into an array.  All this confusion is
    # necessary so that we don't match on something we've already replaced,
    # which can happen if you do multiple s///g operations.

    my @things;
    # Either a comment string or no comma and a compulsory #
    # text is replaced with a link if the bug wasn't moved
    while ($text =~ s/\bbug(\s|%\#)*(\d+)(,?\s*comment\s*)(\s|%\#)(\d+)/"##$count##"/ei) {
        my $item;
        my $bugnum = $2;
        my $comnum = $4;
        
        if (exists $otn_bugs->{$bugnum}) {
            $bugnum = $otn_bugs->{$bugnum};
            $item = "bug$1$bugnum$2$3$4$5"
        } else {
            $item = "${my_db_url1}show_bug.cgi?id=$bugnum#c${comnum}";
        }
        
        $things[$count++] = $item;
    }
    # Bugs (no comment)
    while ($text =~ s/\bbug(\s|%\#)*(\d+)/"##$count##"/ei) {
        my $item = $&;
        my $num = $2;
        
        if (exists $otn_bugs->{$num}) {
            $num = $otn_bugs->{$num};
            $item = "bug$1$num";
        } else {
            $item = "${my_db_url1}show_bug.cgi?id=$num";
        }
        $things[$count++] = $item;
    }
    # showattachment.cgi is the old way of linking to attachments
    # attachment.cgi is the new way
    while ($text =~ s/$my_db_url1_re(?:show)?attachment\.cgi\?(?:attach_)?id=(\d+)/"##$count##"/ei) {
        my $num = $1;
        my $item;
        my $newnum = $otn_attachments->{$num};
        if ($newnum) {
            $item = "${my_db_url2}attachment.cgi?id=$newnum";
        } else {
            $item = $&;
        }
        $things[$count++] = $item;
    }
    while ($text =~ s/${my_db_url1_re}show_bug\.cgi\?id=(\d+)/"##$count##"/ei) {
        # WARNING:
        #   - Doesn't convert the comments (show_bug.cgi?id=123#c5 for bug 123 comment 5)
        #     Fortunately:
        #     1. Ximian bugzilla doesn't contain these
        #     2. Comment numbers should stay the same 
        my $num = $1;
        my $item;
        if (exists $otn_bugs->{$num}) {
            $num = $otn_bugs->{$num};
            $item = "${my_db_url2}show_bug.cgi?id=$num";
        } else {
            $item = $&;
        }
        $things[$count++] = $item;
    }
    # 'Created an attachment' comments and 'attachment 123'
    while ($text =~ s/\b(Created an )?attachment(\s|%\#)*(\(id=)?(\d+)(\))?/"##$count##"/ei) {
        my $item = $&;
        my $num = $4;
        if (exists $otn_attachments->{$num}) {
            $num = $otn_attachments->{$num};
            $item = "$1attachment$2$3$num$5";
        } else {
            # This could only happen for an 'attachment XXX' comment, all
            # created attachments should be moved
            #
            # this, replace the entire thing with a link
            $item = "${my_db_url1}attachment.cgi?id=$num";
        }
        $things[$count++] = $item;
    }
    while ($text =~ s/\*\*\* This bug has been marked as a duplicate of (\d+) \*\*\*/"##$count##"/ei) {
        my $item = $&;
        my $num = $1;
        
        if (exists $otn_bugs->{$num}) {
            $num = $otn_bugs->{$num};
        } else {
            $num = "${my_db_url1}show_bug.cgi?id=$num";
        }
        $item = "*** This bug has been marked as a duplicate of $num ***";
        
        $things[$count++] = $item;
    }
    # Stuff everything back from the array.
    for (my $i=0 ; $i<$count ; $i++) {
        $text =~ s/##$i##/$things[$i]/e;
    }

    # And undo the quoting of "#" characters.
    $text =~ s/%#/#/g;

    return $text;
}

sub fixPriorityAndSeverity {
    my ($oldPriority, $field, $rowRef, $fieldidxRef, $uniq) = @_;

    my $oldSeverity = $rowRef->[$fieldidxRef->{bug_severity}];
    
    # Update Severity
    $rowRef->[$fieldidxRef->{bug_severity}] = $convertToSeverity{$oldPriority};
    
    # Update Priority
    return 'Normal'; # XXX -- ok, this really should be converted!
}


# More helper functions
sub retUndef {}
sub retZero {return 0}
sub retAll {return 'All'}

###########################################################################
# ACTUAL WORK STARTS HERE
###########################################################################

# CHECK DATABASES
if (!CompareTables()) {
    print "Tables are not the same. Exiting\n";
    exit(1);
}
if (!AuditSQL()) {
    print "One or more audit failed. Exiting\n";
#    exit(1);
}

# Fetch groups used in source db
my $groupsRef = DetermineActiveGroups($where);
# Create groups if they do not exist
MoveGroups(values %$groupsRef);
# destination name->bit
%dGroupnameToId = @{$dbh2->selectcol_arrayref("SELECT name, bit FROM groups WHERE isbuggroup = 1",
                                              { Columns=>[1,2] })};


# Get the user_id (profiles) that have to be created
# Currently handles:
#     bugs, bugs_activity, longdescs, attachments, components
# Fetch all used user_id
print "Fetching user_id ";
@old_userids = SelectUnion(\@user_sql, $where, ".");
print scalar @old_userids . "\n";

# XXX -- needed somehow -- should fix this
my %la = %$groupsRef;

# Determine the access to various groups per field. Used by fixGroupset
#$profilesaccess{groupset} = DetermineProfileGroup(\%la, \@old_userids, 'groupset');
#$profilesaccess{blessgroupset} = DetermineProfileGroup(\%la, \@old_userids, 'blessgroupset');


# Limit to the selected userids
# XXX -- existing userids should have their group access updated!


# First move (create) the profiles that do not exist in the destination database
UpdateProfiles(@old_userids);
SetUserHashes(@old_userids);


# Move various tables
UpdateFielddefs();
setFielddefHashes();

UpdateKeyworddefs();
setKeyworddefHashes();

my $where_c2 = MakeWhere("concat(value,\"_-|-_\",program)", "components", "concat(value,\"_-|-_\",program)", "components", "concat(value,\"_-|-_\",program)", $where_components);
if ($where_c2) {
    move_records($dbh1, $dbh2, "components", undef, \%rename_components, $where_c2);
}

my $where_p2 = MakeWhere("product", "products", "product", "products", "product", $where);
if ($where_p2) {
    move_records($dbh1, $dbh2, "products", undef, undef, $where_p2);
}

my $where_versions = MakeWhere("concat(value,\"_-|-_\",program)", "versions", "concat(value,\"_-|-_\",program)", "versions", "concat(value,\"_-|-_\",program)", $where_components);
if ($where_versions) {
    move_records($dbh1, $dbh2, "versions", undef, undef, $where_versions);
}

my $where_milestones = MakeWhere("concat(value,\"_-|-_\",product)", "milestones", "concat(value,\"_-|-_\",product)", "milestones", "concat(value,\"_-|-_\",product)", $where);
if ($where_milestones) {
    move_records($dbh1, $dbh2, "milestones", undef, undef, $where_milestones);
}

# Now move the bugs
$otn_bugs = move_records($dbh1, $dbh2, "bugs", "userid", \%rename_bugs, $where);

# After that, move the cc
move_records($dbh1, $dbh2, "cc", undef, \%rename_cc, "bug_id " . $where_bugids);

# Move attachments
$otn_attachments = move_records($dbh1, $dbh2, "attachments", "attach_id", \%rename_attachments, "bug_id " . $where_bugids);

# Move bugs_activity
move_records($dbh1, $dbh2, "bugs_activity", undef, \%rename_bugs_activity, "bug_id " . $where_bugids);

# Move dependencies
# only move known blocked AND dependson records
move_records($dbh1, $dbh2, "dependencies", undef, \%rename_dependencies, "blocked " . $where_bugids . " AND dependson " . $where_bugids);

# Move duplicates
# only move known blocked AND dupe_of records
move_records($dbh1, $dbh2, "duplicates", undef, \%rename_duplicates, "dupe_of " . $where_bugids . " AND dupe " . $where_bugids);

# Move keywords
move_records($dbh1, $dbh2, "keywords", undef, \%rename_keywords, "bug_id " . $where_bugids);

# Move comments (this included the initial comment)
move_records($dbh1, $dbh2, "longdescs", "bug_id", \%rename_longdescs, "bug_id " . $where_bugids);


print "NOTICE: Delete the file data/versioncache in the destination Bugzilla!\n";

# vim: set expandtab smarttab sw=4 sm:
